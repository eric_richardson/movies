# Cigna Movies Interview

This application displays a list of movies to the user from the [TMBD](https://www.themoviedb.org/) API. It consists of a Node backend and a React frontend.

## Getting Started

To run this application, please have [Docker](https://www.docker.com/) installed on your computer.

Execute the following commands:

- `docker-compose build`
- `docker-compose up` or `docker-compose up -d` if you want it to run silently in the background

If you do not have Docker installed, navigate to [server](./server/README) and [client](./client/README) to get the instructions on how to run each application individually .

'use strict';

const {
  ApolloServer
} = require('apollo-server-express');
const cors = require('cors');
const express = require('express');

const resolvers = require('./graphql/resolvers');
const typeDefs = require('./graphql/typeDefs');
const restRoutes = require('./rest/routes');

const port = process.env.PORT || 5000;
const app = express();

app.use(cors());
// create REST API routes
app.use('/api', restRoutes)

// create apollo graphql server
const apolloServer = new ApolloServer({
  resolvers,
  typeDefs
});

apolloServer.applyMiddleware({ app });

app.listen({ port }, () => {
  console.log(`Server listening on port ${port}`);
});

'use strict';

const { titles } = require('../../services');

const resolvers = {
  Query: {
    title: (root, args) => {
      const { id } = args;
      return titles.getTitle(id)
    },
    titles: () => titles.getTitles()
  }
};

module.exports = resolvers;

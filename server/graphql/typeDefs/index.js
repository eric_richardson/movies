'use strict';

const root = require('./root');
const title = require('./title');

module.exports = [root, title];

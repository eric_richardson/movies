'use strict';

const { gql } = require('apollo-server-express');

module.exports = gql`
  extend type Query {
    titles: [Title!]!,
    title(id: ID!): Title!
  }

  type Title {
    id: ID!
    title: String!,
    thumbnail: String!
  }
`;

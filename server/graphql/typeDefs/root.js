'use strict';

const { gql } = require('apollo-server-express');

// sets the base graphql root Query
module.exports = gql`
  type Query {
    _: String
  }
`;

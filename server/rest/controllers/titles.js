'use strict';

const { titles } = require('../../services')

const collection = function(req, res, next) {
  titles.getTitles().then((movies) => res.json(movies)).catch((err) => next(err))
}

const findOne = function(req, res, next) {
  titles.getTitle(req.params.id).then((movie) => res.json(movie)).catch((err) => next(err))
}

module.exports = {
  collection,
  findOne
};

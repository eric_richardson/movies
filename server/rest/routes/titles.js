'use strict';

const express = require('express');
const { titles } = require('../controllers');

const router = express.Router();

// get all titles
router.get('/', titles.collection);
// get a title by ID
router.get('/:id', titles.findOne);

module.exports = router;

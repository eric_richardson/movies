'use strict';

const express = require('express');

const titlesRouter = require('./titles');

const router = express.Router();

router.use('/titles', titlesRouter);

module.exports = router;
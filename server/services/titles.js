'use strict';

const axios = require('axios');

class Titles {
  // gets all "popular" movie titles
  getTitles() {
    return axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}`).then((res) => 
      res.data && res.data.results.map((movie) => (
        { 
          id: movie.id, 
          title: movie.title, 
          thumbnail: `https://image.tmdb.org/t/p/original/${movie.poster_path}` 
        }
      ))
    )
  }

  // gets a single title by movie ID
  getTitle(id) {
    return axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.API_KEY}`).then((res) =>
      res.data && { 
        id: res.data.id, 
        title: res.data.title, 
        thumbnail: `https://image.tmdb.org/t/p/original/${res.data.poster_path}`
      }
    )
  }
}

module.exports = Titles;

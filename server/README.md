# Cigna Movies Server

Express server with GraphQL and REST endpoints.

## Getting Started

Please have >= Node 14 installed on your machine to run this application.

[Yarn](https://yarnpkg.com/) is used to install packages.

### Without Docker

- `yarn` or `npm i` to install all dependencies.
- `yarn start` or `npm start` to start the server. To run nodemon in development mode execute `yarn dev` or `npm run dev`.

Note: Please look at the ENV variables in the [Dockerfile](./Dockerfile) needed to run this application if running this way.

### With Docker

- `docker build -t cigna_express_server .`
- `docker run -it -p 5000:5000 cigna_express_server`

The REST API can be reached `http://localhost:5000/api` and the GrahQL API can be reached `http://localhost:5000/graphql`

Then navigate to `http://localhost:3000/` to view the application.

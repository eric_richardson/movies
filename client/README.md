# Cigna Movies Client

ReactJS frontend application that displays a list of movies from a GraphQL endpoint.

## Getting Started

### Without Docker

- `yarn` or `npm i` to install all dependencies.
- `yarn start` or `npm start` to start the client.

### With Docker

- `docker build -t cigna_movie_client .`
- `docker run -it -p 3000:3000 cigna_movie_client`

The application can be reached `http://localhost:3000/`. It will require the server to be running on `http://localhost:5000` with a graphql endpoint available at `/graphql`

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

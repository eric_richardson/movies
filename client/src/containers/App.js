import { gql, useQuery } from '@apollo/client';
import MovieList from '../components/MovieList';
import TitleBar from '../components/TitleBar'

const GET_TITLES = gql`
  {
    titles {
      id
      title
      thumbnail
    }
  }
`;

const App = () => {
  // fetch all movie titles
  const { error, data, isLoading } = useQuery(GET_TITLES);

  return (
    <>
      <TitleBar title="Cigna Movies" />
      {isLoading ? <div>Loading...</div> : null}
      {error ? <div>Error!</div> : null}
      {data ? <MovieList movies={data.titles} /> : null}
    </>
  );
};

export default App;

import PropTypes from 'prop-types';
import styled from 'styled-components';

const propTypes = {
  background: PropTypes.string,
  color: PropTypes.string,
  padding: PropTypes.number,
  title: PropTypes.string.isRequired
};

const defaultProps = {
  background: 'blue',
  color: 'white',
  padding: 10
};

const TitleWrapper = styled.div`
  background-color: ${props => props.background};
  color: ${props => props.color};
  display: flex;
  padding: ${props => props.padding}px;
`;

const TitleBar = ({ background, color, padding, title }) => (
  <TitleWrapper
    padding={padding}
    background={background}
    color={color}
  >
    <h1>{title}</h1>
  </TitleWrapper>
);

TitleBar.defaultProps = defaultProps;
TitleBar.propTypes = propTypes;

export default TitleBar;

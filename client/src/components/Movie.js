import PropTypes from 'prop-types';
import styled from 'styled-components';

const propTypes = {
  className: PropTypes.string,
  movie: PropTypes.shape({
    thumbnail: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  }).isRequired
};

const CardWrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  height: 500px;
  width: 300px;
  margin: 10px;
`;

const Image = styled.img`
  max-height:100%;
  max-width:100%;
`;

const Heading = styled.p`
  font-size: 16px;
  font-weight: 700;
`;

const Movie = ({ className, movie }) => (
  <CardWrapper className={className}>
    <Image src={movie.thumbnail} />
    <Heading>{movie.title}</Heading>
  </CardWrapper>
);

Movie.propTypes = propTypes;

export default Movie;

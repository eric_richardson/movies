import PropTypes from 'prop-types';
import styled from 'styled-components'
import Movie from './Movie';

const propTypes = {
  movies: PropTypes.array.isRequired
};

const ListWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const MovieList = ({ movies }) => (
  <ListWrapper>
    {movies.map((movie) => <Movie key={movie.id} movie={movie} />)}
  </ListWrapper>
);

MovieList.propTypes = propTypes;

export default MovieList;
